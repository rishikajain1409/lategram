import "../Style/Photoupload.css";
import { useState } from "react";
import axios from "axios";

import {  useNavigate } from "react-router-dom";

const Photoupload=()=>{
  const ppimg="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSZjku55GSY0BMpw5eibGDx8jBpC4YJ9vopuqH4n8C4g&usqp=CAU&ec=48665699";
  const [profilephoto1,setProfilephoto]=useState(ppimg)
  const [profilephoto2,setProfilephoto2]=useState(ppimg)
  const userid = localStorage.getItem("userid");
   const navigate=useNavigate();
  const handleFileInputChange = (event) => {
    const profilephoto = event.target.files[0];
    console.log(profilephoto,"pro")
    setProfilephoto2(profilephoto);
    setProfilephoto(URL.createObjectURL(profilephoto));
      // localStorage.setItem('selectedFile', profilephoto);
    

  }

  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };

  const uploadProfilephoto=(event)=>{
    event.preventDefault();
    var formData = new FormData();
    formData.append("upload", profilephoto2);
    formData.append("profilephotoname", profilephoto2.name);
    const baseURL=`https://lgbe.onrender.com/image/${userid}`;
    axios.post(baseURL,formData,config)
      .then(response => {
        console.log(response.data);
        navigate('/home')
      })
      .catch(error => {
        console.log(error);
      })
}
  

return(<>
<div id="wrapper11">
        <div className="main-content11">
        <div className="d-flex justify-content-center">
              <img src={profilephoto1} alt="Profilephoto" class="d-flex justify-content-center" style={{borderRadius:60}} />
            </div>

<label for="file-input" className="label123">
 
<img src="https://www.freeiconspng.com/uploads/upload-icon-30.png" alt="" className="photopu"/>

</label>
<form  enctype="multipart/form-data" onSubmit={uploadProfilephoto}>
<input id="file-input" type="file" name="upload" className="ppc" onChange={handleFileInputChange} />
<input type="submit" value="upload" className="btn" />
</form>
</div>
	
            
        </div>

</>)
}
export default Photoupload;