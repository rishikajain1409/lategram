import "../Style/NavBar.css";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import HomeSharpIcon from '@mui/icons-material/HomeSharp';
import { IconButton } from '@mui/material';
import {NotificationsActiveRounded,MarkChatUnreadSharp,UploadSharp,InsertEmoticonSharp,LogoutSharp} from '@mui/icons-material';
import { useEffect, useState } from "react";
import axios from "axios";
const NavBar =()=>{
  const ppimg="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSZjku55GSY0BMpw5eibGDx8jBpC4YJ9vopuqH4n8C4g&usqp=CAU&ec=48665699";
  const [profilephoto1,setProfilephoto]=useState(ppimg)
  
  


const navigate = useNavigate()
  const state =useSelector((state)=>state.userReducer);
  // const name=state.username;
  const userid = localStorage.getItem("userid");
  const name=localStorage.getItem("firstname")
  const id= state.userid;
 
const onClickMyProfile = (id) =>{
  navigate(`/myprofile/${id}`)
}

// useEffect(()=>{
//   const baseURL=`http://localhost:5005/profileimage/${userid}`
//   axios.get(baseURL,
//     {
     
//       "Content-Type": "image/jpg",
//       // "Content-Type": "application/json",
     
//       "Access-Control-Allow-Origin": "*",
     
//      }
  
//   ).then(res => {
//     console.log(res,"like response")
//     // console.log(res.data.profilephoto,"like response123456768")
//     setProfilephoto(res)
  
//   }).catch((error)=>{
//     console.log(error)
    
//   })
// },[])

useEffect(() => {
  const fetchData = async () => {
    try {
      const response = await axios.get(`http://localhost:5005/profileimage/${userid}`, {
        responseType: 'arraybuffer',
      });
      const data= response.data;
      const base64=btoa(
        new Uint8Array(data).reduce(
          (data,byte)=>data +String.fromCharCode(byte),
          '',
        ),
      );
      const src= `data:image/jpeg;base64,${base64}`;
      setProfilephoto(src);
      // const buffer = Buffer.from(response.data, 'binary').toString('base64');
      // setProfilephoto(`data:image/jpeg;base64,${buffer}`);
    } catch (error) {
      console.log(error);
    }
  };

  fetchData();
}, []);



return(
    <header className="grid main-header">
    <div className="flex-container header-container">
    <div >
<IconButton>
<label for="file-input" className="label123">
<Link to="/photoupload"> 
<img src={profilephoto1} alt="" className="photo"/>
</Link>
</label>
{/* <form type="submit">
<input id="file-input" type="file" className="ppc"  onChange={handleFileInputChange}/>
</form> */}

</IconButton>
</div>
      <span className="logo logo-nav header-item">
         Welcome , {name}
      </span>
    
    <nav className="header-item main-nav">
      <ul className="navbar flex-container">
        <li className="navbar-item">
         <i className="bi bi-house-door-fill"></i>
        </li>
        <li className="navbar-item">
          {/* <i className="bi bi-send"></i> */}
          <Link to="/home"><HomeSharpIcon sx={{fontSize:40}}/></Link>
        </li>
        <li className="navbar-item">
        <MarkChatUnreadSharp sx={{fontSize:30}}/>
        </li>
        <li className="navbar-item">
        <Link to="/notification" ><NotificationsActiveRounded sx={{fontSize:40}}/></Link>
        </li>
        <li className="navbar-item">
        <Link to="/uploadpost"><UploadSharp sx={{fontSize:40}} /></Link>
        </li>
        <li onClick={(e)=>onClickMyProfile(id)} className="navbar-item no-hover">
        {/* <Link to={`/myprofile/${id}`}> lonclcik={(e)=>function(id)} */}
        <InsertEmoticonSharp sx={{fontSize:40}} />
        {/* </Link> */}
        </li>
        
        <li className="navbar-item">
        <Link to="/logout" >
        <LogoutSharp sx={{fontSize:40}}/>
          </Link>
        </li>
      </ul> 
    </nav>
    </div>
  </header>
  
  
);
}
export default NavBar;