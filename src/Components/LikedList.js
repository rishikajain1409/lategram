import "../Style/ComentList.css";
import NavBar from "./NavBar";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {FavoriteRounded,ChatBubbleSharp,SendRounded} from '@mui/icons-material';

const LikedList =()=>{
  const [commentlist,setCommentList]=useState([]);
  const [img,setImg]=useState();
  const { id } = useParams();
  //console.log(id,"git the od")
  useEffect(() => {
    const baseURL = `https://lgbe.onrender.com/getlikes/${id}`;
    axios.get(baseURL,{
      headers:{
         "Content-Type":"application/json",
         Accept:"application/json",
         "Acess-Control-Allow-Origin":"",
    
    }
    }).then(res => {
    // console.log(res)
    console.log(res.data,"post result")
    const local = res.data.LikedBy;
    setCommentList(local);
    setImg(res.data.img_url);
    
    
    }).catch((error)=>{
    console.log(error)
    
    // alert(error.response.data.msg)
    })  
  
  }, []);

console.log(commentlist,"liked by list")

return(<>
<NavBar/>
<div id="wrapper1">
        <div className="main-content1">
          <div className="header1">
            <h1 className="textItalic1">Likes on your post</h1>
            <div class="main-gallery-wrapper1 flex-container1">
            <div class="pop-img-container1">
              <img src= {img} alt="" class="pop-img" />
            </div>
          </div>
            {commentlist.map((item,i)=>{
              console.log(item,"-------------")
              //return null;
              return(
                <div className="padding">
            <p>Liked by {item}</p>
            <div className="seperator"></div>
            </div>
              )
            })}
            
          </div>
          
            </div>
        </div>
</>);
}
export default LikedList;